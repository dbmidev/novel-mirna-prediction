#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $output = "";
my $species = "";
my $max_energy = -4.30;
my $min_stem_pairings = 14;
my $min_length = 41;
my $max_length = 180;

GetOptions ('input|i=s' => \$input,
			'output|o=s' => \$output,
			'species|s=s' => \$species,
			'energy|e=f' => \$max_energy,
			'pairings|p=i' => \$min_stem_pairings,
			'minLength|min=i' => \$min_length,
			'maxLength|max=i' => \$max_length);

if ($input eq "" || $output eq "") {
	print "Specify input and/or output file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;
open (OUT, ">", $output) or die $!;

print fileparse($input)."\n";

while (<IN>) {

	my $line = $_;
	
	if ($line =~ /^>.*$species.*/i) { #header
		
		my $seq = <IN>; #sequence
		
		if ($min_length <= length($seq) && $max_length >= length($seq)) {		
			my $str = <IN>; #structure
			chomp $str;
			
			my $basePairingsStem = ($str =~ tr/\(//);
			if ($min_stem_pairings <= $basePairingsStem) {
				if ($str =~ /(.*)\t(.*)/ && $max_energy >= $2) {
					print OUT $line;
					print OUT $seq;
					print OUT $str."\n";
				}
			}
		}		
	}
}

close IN;
close OUT;