#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $species = "";

GetOptions ('input|i=s' => \$input,
			'species|s=s' => \$species);

if ($input eq "") {
	print "Specify input file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;

print fileparse($input)."\n";

my $min_length = 1000;
my $max_length = -1000;

my %lengths = ();

while (<IN>) {

	my $line = $_;
	
	if ($line =~ /^>.*$species.*/i) { #header
		
		my $seq = <IN>; #sequence
		chomp $seq;
		
		my $length = length($seq);
		
		$lengths{$length}++;
		<IN>;
		
		if ($length > $max_length) {
			$max_length = $length;
		}
		if ($length < $min_length) {
			$min_length = $length;
		}
	}
}

close IN;

print "min_length\t".$min_length."\n";
print "max_length\t".$max_length."\n";

foreach my $length (sort {$a <=> $b} keys %lengths) {
	print $length."\t".$lengths{$length}."\n";
}

print "all\n";
for (my $i = $min_length; $i <= $max_length; $i++) {
	if (exists $lengths{$i}) {
		print $i."\t".$lengths{$i}."\n";
	} else {
		print $i."\t0\n";
	}
}