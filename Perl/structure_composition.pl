#!/usr/bin/perl
# 02/23/2012
# Author: Petra Stepanowsky
#
# Script to

# >mmu-mir-5119 MI0018028 Mus musculus miR-5119 stem-loop
# CAGGGCUGGCCUAUGGGACAGAUACUAUAAGUCAUCUCAUCCUGGGGCUGGGUCUGC
# ( ( ( ( . ( . . ( (  (  (  .  (  (  (  (  (  .  (  (  (  (  (  (  .  .  .  .  .  )  )  .  )  )  )  )  .  .  )  )  )  )  )  )  )  )  )  .  .  )  .  )  )  )  )  .	-22.00
# 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56

use strict;
use warnings;
use Clone qw(clone);
use Getopt::Long;

my $input = "";
my $output = "";
my $file = "";

GetOptions ('input|i=s' => \$input,
			'output|o=s' => \$output,
			'tab|t=s' => \$file);

if ($input eq "" || $output eq "" || $file eq "") {
	print "Specify input and/or output file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;
open (OUT, ">", $output) or die $!;
open (TAB, ">", $file) or die $!;

my %triplet = ("A" => {"000" => 0, "100" => 0, "010" => 0, "001" => 0, "101" => 0, "011" => 0, "110" => 0, "111" => 0},
			   "C" => {"000" => 0, "100" => 0, "010" => 0, "001" => 0, "101" => 0, "011" => 0, "110" => 0, "111" => 0},
			   "G" => {"000" => 0, "100" => 0, "010" => 0, "001" => 0, "101" => 0, "011" => 0, "110" => 0, "111" => 0},
			   "U" => {"000" => 0, "100" => 0, "010" => 0, "001" => 0, "101" => 0, "011" => 0, "110" => 0, "111" => 0});
			   
my @cat = ("l", "m", "r");
		
print TAB "id\tnum_l\tnum_m\tnum_r";

foreach my $c (@cat) {		

	foreach my $nt (keys %triplet) {
		foreach my $t (keys %{$triplet{$nt}}) {
			print TAB "\t".$nt.$t."_".$c;
		}
	}
}
print TAB "\n";
			   
while (<IN>) {

	my $line = $_;
	if ($line =~ /^>/) { #header
	
		my $sequence = <IN>; #sequence
		chomp $sequence;
		
		my $structure = <IN>; #structure
		chomp $structure;
		
		if ($structure =~ /(\().*\((\.*)\).*(\))/) {
			my $stembegin = $-[1];
			my $stemend = $-[3];
			my @stemloop = ($-[2], $+[2]-1);
			
			# print "begin: $stembegin\n";
			# print "end: $stemend\n";
			# print "loop: @stemloop\n";
			
			my %mTriplet = %{clone(\%triplet)};
			my %rTriplet = %{clone(\%triplet)};
			my %lTriplet = %{clone(\%triplet)};
			
			my $num_mtriplets = 0;
			my $num_ltriplets = 0;
			my $num_rtriplets = 0;
	
			for (my $i = 1; $i < length($sequence) - 1; $i++) {
				my $structure_triplet = substr($structure, $i - 1, 3);
				$structure_triplet =~ s/\)/\(/g;
				$structure_triplet =~ s/\(/1/g;
				$structure_triplet =~ s/\./0/g;
				
				if (($i >= $stembegin) && ($i < $stemloop[0] || $i > $stemloop[1]) && ($i <= $stemend)) {
					#print substr($sequence, $i, 1).$structure_triplet."\n";
					$mTriplet{substr($sequence, $i, 1)}{$structure_triplet}++;
					$num_mtriplets++;
				}	

				if (($i > $stembegin) && ($i <= $stemloop[0] || $i > $stemloop[1] + 1) && ($i <= $stemend + 1)) {
					#print substr($sequence, $i - 1, 1).$structure_triplet."\n";
					$lTriplet{substr($sequence, $i - 1, 1)}{$structure_triplet}++;
					$num_ltriplets++;
				}	

				if (($i >= $stembegin - 1) && ($i < $stemloop[0] - 1 || $i >= $stemloop[1]) && ($i < $stemend)) {
					#print substr($sequence, $i + 1, 1).$structure_triplet."\n";
					$rTriplet{substr($sequence, $i + 1, 1)}{$structure_triplet}++;
					$num_rtriplets++;
				}					
			}
			
			print OUT $line;
			print OUT $sequence."\n";
			print OUT $structure."\n";
			
			my $first = 1;
			
			foreach my $nt (keys %mTriplet) {
				foreach my $triplet (keys %{$mTriplet{$nt}}) {
					if (!$first) {
						print OUT "\t";
					} else {
						$first = 0;
					}
					print OUT $nt.$triplet;
				}
			}
			
			print OUT "\n";
			
			$first = 1;
			
			$line =~ /^>(.*?)\s/;
			my $id = $1;
			print TAB $id."\t".$num_ltriplets."\t".$num_mtriplets."\t".$num_rtriplets;
				
			foreach my $nt (keys %lTriplet) {
				foreach my $triplet (keys %{$lTriplet{$nt}}) {
					if (!$first) {
						print OUT "\t";
					} else {
						$first = 0;
					}
					printf OUT ("%.4f",$lTriplet{$nt}{$triplet} / $num_ltriplets);
					print TAB "\t".($lTriplet{$nt}{$triplet} / $num_ltriplets);
				}
			}
			
			print OUT "\n";
			
			$first = 1;
			
			foreach my $nt (keys %mTriplet) {
				foreach my $triplet (keys %{$mTriplet{$nt}}) {
					if (!$first) {
						print OUT "\t";
					} else {
						$first = 0;
					}
					printf OUT ("%.4f",$mTriplet{$nt}{$triplet} / $num_mtriplets);
					print TAB "\t".($mTriplet{$nt}{$triplet} / $num_mtriplets);
				}
			}
			
			print OUT "\n";
			
			$first = 1;
			
			foreach my $nt (keys %rTriplet) {
				foreach my $triplet (keys %{$rTriplet{$nt}}) {
					if (!$first) {
						print OUT "\t";
					} else {
						$first = 0;
					}
					printf OUT ("%.4f",$rTriplet{$nt}{$triplet} / $num_rtriplets);
					print TAB "\t".($rTriplet{$nt}{$triplet} / $num_rtriplets);
				}
			}
			
			print OUT "\n";
			print TAB "\n";
		}		
	}
}

close IN;
close OUT;
close TAB;

# my $sequence  = "CAGGGCUGGCCUAUGGGACAGAUACUAUAAGUCAUCUCAUCCUGGGGCUGGGUCUGC";
# my $structure = "((((.(..((((.(((((.((((((.....)).))))..)))))))))..).)))).";














