#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $species = "";

GetOptions ('input|i=s' => \$input,
			'species|s=s' => \$species);

if ($input eq "") {
	print "Specify input file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;

my @pairLength = ();

while (<IN>) {

	my $line = $_;
	
	if ($line =~ /^>.*$species.*/i) { #header
		
		my $seq = <IN>; #sequence
		chomp $seq;
		
		my $str = <IN>; #structure
		chomp $str;
		
		if ($str =~ /(\().*\((\.*)\).*(\))\.*[\s\t]/) {
			my $stembegin = $-[1];
			my $stemend = $-[3];
			my @stemloop = ($-[2], $+[2]-1);
			
			my $loop_length = $stemloop[1] - $stemloop[0] + 1;
			push(@pairLength, [length($seq), $loop_length]);
		}
	}
}

close IN;

print "length\tloop\n";

foreach my $pair (@pairLength) {
	print ${$pair}[0]."\t".${$pair}[1]."\n";
}