#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $species = "";

GetOptions ('input|i=s' => \$input,
			'species|s=s' => \$species);

if ($input eq "") {
	print "Specify input file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;

print fileparse($input)."\n";

my $min_base_pairing = 1000;
my $max_base_pairing = -1000;

my %base_pairings = ();

while (<IN>) {

	my $line = $_;
	
	if ($line =~ /^>.*$species.*/i) { #header
		
		my $seq = <IN>; #sequence
		
		my $str = <IN>; #structure
		chomp $str;
		
		my $basePairingsStem = ($str =~ tr/\(//);
		
		$base_pairings{$basePairingsStem}++;
		
		if ($basePairingsStem > $max_base_pairing) {
			$max_base_pairing = $basePairingsStem;
		}
		if ($basePairingsStem < $min_base_pairing) {
			$min_base_pairing = $basePairingsStem;
		}
	}
}

close IN;

print "min_stem_pairing\t".$min_base_pairing."\n";
print "max_stem_pairing\t".$max_base_pairing."\n";

foreach my $basePairing (sort {$a <=> $b} keys %base_pairings) {
	print $basePairing."\t".$base_pairings{$basePairing}."\n";
}

print "all\n";
for (my $i = $min_base_pairing; $i <= $max_base_pairing; $i++) {
	if (exists $energies{$i}) {
		print $i."\t".$base_pairings{$i}."\n";
	} else {
		print $i."\t0\n";
	}
}