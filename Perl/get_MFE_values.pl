#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $random = "";
my $output = "";
my $iterations = 1000;

GetOptions ('input|i=s' => \$input,
			'output|o=s' => \$output,
			'random|r=s' => \$random,
			'iterations|k=i' => \$iterations);

if ($input eq "" || $output eq "" || $random eq "") {
	print "Specify input and/or output file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;

my %mfe = ();

while (<IN>) {
	my $line = $_;
	
	if ($line =~ /^>(.*?)\s/) { #header
		my $id = $1;
		
		my $seq = <IN>; #sequence
		$seq = clean_line($seq); 
		
		my $str = <IN>; #structure
		
		if ($str =~ /(.*)\t(.*)/) {
			my $energy = $2;
			$energy = clean_line($energy);
			my $energy_norm = $energy / length($seq);
			
			$mfe{$id} = [$energy, length($seq)];
		}	
	}
}

close IN;


open (RAND, "<", $random) or die $!;

my %mfe_rand = ();

foreach my $id (keys %mfe) {
	$mfe_rand{$id} = 0;
}

while(<RAND>) {
	my $line = $_;
	
	if ($line =~ /^>(.*?)\s/) { #header
		my $id = $1;
		
		my $seq = <RAND>; #sequence
		$seq = clean_line($seq); 
		
		my $str = <RAND>; #structure
		
		if ($str =~ /(.*)\s[\(](.*)[\)]/) {
			my $energy = $2;
			$energy = clean_line($energy);
			
			if ($energy <= $mfe{$id}[0]) {
				$mfe_rand{$id}++;
			}
		}	
	}
}

close RAND;

open (OUT, ">", $output) or die $!;

print OUT "id\tMFE\tMFE_pVal\tMFE_norm\n";

foreach my $id (keys %mfe) {
	print OUT $id."\t";
	print OUT $mfe{$id}[0]."\t";
	print OUT ($mfe_rand{$id}/$iterations)."\t";
	print OUT ($mfe{$id}[0]/$mfe{$id}[1])."\n";
}


close OUT;

sub clean_line {
	my $text = $_[0];
	$text =~ s/[\r\n]//g;
	return $text;
}