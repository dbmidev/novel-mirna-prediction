#!/usr/bin/perl
# 02/17/2012
# Author: Petra Stepanowsky
# 
# Script to generate random sequences 
# (Random Doublet-preserving Permutation Algorithm, Altschul S.F. and Erickson B.W. 1985)

use strict;
use warnings;
use Clone qw(clone);
use Getopt::Long;

my $usage = << "USAGE";
Description: Perl script used to generate random sequences (Random Doublet-preserving Permutation Algorithm, Altschul and Erickson, 1985)
Author: Petra Stepanowsky
Usage: perl randomSequences.pl [options]
Options:
  --help,       --h         help
  --input,      --i  <str>  one or more sequences, ',' separated
  --file,       --f  <file> FASTA formatted file with input sequences
  --iterations, --k  <int>  Number of generated random sequences, default: 1
Examples: perl randomSequences.pl --input AGACATAAAGTTCCGTACTGCCGGGAT
          perl randomSequences.pl --file input.fa --k 100
USAGE

my $help       =  0; # if specified, show usage
my $input      = ""; # input sequence/s
my @sequences  = (); # sequences
my $file       = ""; # file containing sequences in FASTA format
my $iterations =  1; # number of generated sequences
my $noheader   =  0; # if 1, no header is printed

# possible command options
GetOptions ('help|h'         => \$help,
            'input|i=s'      => \$input,
			'file|f=s'       => \$file,
			'iterations|k=i' => \$iterations,
			'noheader|nh'     => \$noheader);
			
#################
# print usage if the option 'help' is set or if no sequence or input file is specified
if ($help || ($input eq "" && $file eq "")) {
	print $usage;
	exit;
}
#################

if ($input ne "") {
	my $cnt = 1;
	for my $seq (split(/,/, $input)) {
		my @pair = ($seq, ">seq$cnt");
		push (@sequences, \@pair);
		$cnt++;
	}
} elsif ($file ne "") {
	my $cnt = 1;
	open (IN, "<", $file) or die $!;
	while (<IN>) {
		my $line = $_;
		chomp $line;
		if ($line =~ /^>/) {
			my $seq = <IN>;
			chomp $seq;
			my $pair = [$seq, $line];
			push (@sequences, $pair);
			$cnt++;
		} else {
			my $header = ">seq$cnt";
			my $pair = [$line, $header];
			push (@sequences, $pair);
			$cnt++;
		}
	}
	close IN;
}

foreach my $set (@sequences) {

	my $S = ${$set}[0];

	my %E = (); # edge-list E
	my %G = (); # graph G

	my $sb = substr($S,  0, 1); # first character of sequence
	my $sf = substr($S, -1, 1); # last character of sequence 


	# for debugging
	print "sf: ".$sf."\n";
	print "sb: ".$sb."\n";

	#################
	# 1.) construct edge list E(S) for each vertex and the graph G(S)
	for (my $i = 0; $i < length($S) - 1; $i++) {
		push (@{$E{substr($S, $i, 1)}}, substr($S, $i + 1, 1));
		$G{substr($S, $i, 1)}{substr($S, $i + 1, 1)}++;
	}
	#################

	# for debugging
	print "\nedge list E:\n";
	foreach my $key (keys %E) {
		print $key.": ";
		print "@{$E{$key}}\n";
	}

	# for debugging
	print "\ngraph G: \n";
	foreach my $key (keys %G) {
		print $key.":";
		foreach my $vertex (keys %{$G{$key}}) {
			print " ".$vertex;
		}
		print "\n";
	}

	#################
	# generate random sequences of given E(S) and G(S)
	for (my $k = 0; $k < $iterations; $k++) {
		my %Z = ();
		my %permutedE = ();

		#################
		# choose last-edges which are connected to sf
		my $connected = 0;
		while (!$connected) {
			%permutedE = %{clone(\%E)};
			$connected = 1;
			%Z = ();
			
			#################
			# 2.) randomly select 1 edge per vertex s, except vertex sf
			foreach my $s (keys %E) {
				if ($s ne $sf) {
					my $rand = int rand @{$E{$s}};
					my $r = $E{$s}[$rand];
					# 3.) construct last-edge graph Z
					$Z{$s}{$r}++;
					# delete chosen last edges from edge list
					splice (@{$permutedE{$s}}, $rand, 1);
				}	
			}
			#################
			
			#################
			# 4.) check if all vertices are connected to sf
			foreach my $vertex (keys %Z) {
				my %tmp = ();
				my $found = isSfConnected($vertex, \%Z, \%tmp, $sf);
				$connected = $connected && $found;
			}
			#################
		}
		#################

		# for debugging
		print "\ngraph Z: \n";
		foreach my $key (keys %Z) {
			print $key.":";
			foreach my $vertex (keys %{$Z{$key}}) {
				print " ".$vertex;
			}
			print "\n";
		}

		#################
		# 5a.) for each vertex s randomly permute remaining edges of E(S) -> new edge list E(S')
		foreach my $s (keys %permutedE) {
			fisher_yates_shuffle($permutedE{$s})
		}
		#################

		#################
		# 5b.) add last edges to permuted edge list
		foreach my $vertex (keys %Z) {
			push (@{$permutedE{$vertex}}, (keys %{$Z{$vertex}})[0]);
		}
		#################

		# for debugging
		print "\nedge list E':\n";
		foreach my $key (keys %permutedE) {
			print $key.": ";
			print "@{$permutedE{$key}}\n";
		}

		#################
		# 6.) construct (and print) sequence S'
		my $Sprime = $sb;
		my $vertex = $sb;
		for (my $i = 0; $i < length($S) - 1; $i++) {	
			$vertex = shift(@{$permutedE{$vertex}});
			$Sprime .= $vertex;
		}
		
		# print header
		print ${$set}[1]." ".($k + 1)."\n" if (!$noheader);
		#print random sequence
		print $Sprime."\n";
		#################
	}
	#################
}


#########################################
########       SUBROUTINES       ########
#########################################

##############################
# Fisher�Yates shuffle (http://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle)
# generates a random permutation of a given array
# input: reference of array
sub fisher_yates_shuffle {
    my $array = shift;
    for (my $i = $#$array; $i > 0; --$i) {
        my $j = int rand ($i + 1);
        next if $i == $j;
        @{$array}[$i,$j] = @{$array}[$j,$i];
    }
}
##############################
 
 ##############################
 # Checks if there is a path from a given vertex to sf
 # input: starting vertex, graph Z and temporary hash of already visited vertices
 # output: 1 if there is a path, 0 if there is no path
sub isSfConnected {
	my $vertex = $_[0];
	my %graph = %{$_[1]};
	my %tmp = %{$_[2]};
	my $sf = $_[3];
	
	if ($vertex ne $sf) {
		if (exists $graph{$vertex}{$sf}) {
			return 1;
		} else {
			my @keys = keys %{$graph{$vertex}};
			my $found = 0; 
			for (my $i = 0; $i < $#keys+1 && !$found; $i++) {
				if ($keys[$i] ne $vertex) {
					if (not exists $tmp{$vertex}{$keys[$i]}) {
						$tmp{$vertex}{$keys[$i]}++;
						$found = isSfConnected($keys[$i], \%graph, \%tmp, $sf);
					}
				}
			}	
			return $found;
		}
	} 
	return 1;
}
##############################