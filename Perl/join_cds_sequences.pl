#!/usr/bin/perl

use warnings;
use strict;

open (IN, "<", "cds_noAltEvents.txt") or die $!;
open (OUT, ">", "cds_noAltEvents_joined.txt") or die $!;

while (<IN>) {

	my $line = $_;
	
	chomp $line;
	
	unless ($line =~ /^>/) {
		print OUT $line;
	}
}

close IN;
close OUT;