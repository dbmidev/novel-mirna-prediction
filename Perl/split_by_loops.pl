#!/usr/bin/perl
# 02/22/2012
# Author: Petra Stepanowsky
# 
# Script to split given sequences into multiple loop and single loop sequences

use strict;
use warnings;
use Getopt::Long;

my $usage = << "USAGE";
Description: Perl script used to split given secondary structures into multiple- and single-stem loops
Author: Petra Stepanowsky
Usage: perl splitLoops.pl [options]
Options:
  --help,       --h         help
  --input,      --i  <file> input file name, RNAfold output format with header
  --single,     --s  <file> output file name for sequences with single-stem loops
  --mutliple,   --m  <file> output file name for sequences with multiple-stem loops
Examples: perl splitLoops.pl --input input.txt --single singleSeq.txt --multiple multipleSeq.txt
USAGE

my $help     =  0; # if specified, show usage
my $input    = ""; # input file name, RNAfold output format with header
my $single   = ""; # output file name (single-stem loops)
my $multiple = ""; # output file name (multiple-stem loops)

# possible command options
GetOptions ('help|h'       => \$help,
            'input|i=s'    => \$input,
			'single|f=s'   => \$single,
			'multiple|m=s' => \$multiple);
			
#################
# print usage if the option 'help' is set or if no sequence or input file is specified
if ($help || ($input eq "" && $single eq "" && $multiple eq "")) {
	print $usage;
	exit;
}
#################

open (IN, "<", $input) or die $!;
open (SINGLE, ">", $single) or die $!;
open (MULTIPLE, ">", $multiple) or die $!;

while (<IN>) {
	my $header = $_;
	if ($header =~ /^>/) {
		my $sequence = <IN>;
		if ($sequence =~ /[A|C|G|T|N|U|a|c|g|t|n|u]+/) {
			my $structure_line = <IN>;
			if ($structure_line =~ /(.*)\s\(\s?(.*)\)/) {
				my $structure = $1;
				my $energy = $2;
				if ($structure =~ /\).*\(/) {
					# multiple stem-loops
					print MULTIPLE $header;
					print MULTIPLE $sequence;
					print MULTIPLE $structure."\t";
					print MULTIPLE $energy."\n";
				} else {
					#single stem-loops
					print SINGLE $header;
					print SINGLE $sequence;
					print SINGLE $structure."\t";
					print SINGLE $energy."\n";
				}
			}
		}
	}
}

close IN;
close SINGLE;
close MULTIPLE;