#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $species = "";

GetOptions ('input|i=s' => \$input,
			'species|s=s' => \$species);

if ($input eq "") {
	print "Specify input file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;

print fileparse($input)."\n";

my @pairLength = ();

while (<IN>) {

	my $line = $_;
	
	if ($line =~ /^>.*$species.*/i) { #header
		
		my $seq = <IN>; #sequence
		chomp $seq;
		
		my $str = <IN>; #structure
		chomp $str;
		
		my $basePairingsStem = ($str =~ tr/\(//);
		push(@pairLength, [length($seq), $basePairingsStem]);
	}
}

close IN;

print "length\tstem_pairings\n";

foreach my $pair (@pairLength) {
	print ${$pair}[0]."\t".${$pair}[1]."\n";
}