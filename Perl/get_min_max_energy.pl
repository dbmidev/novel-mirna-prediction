#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $species = "";

GetOptions ('input|i=s' => \$input,
			'species|s=s' => \$species);

if ($input eq "") {
	print "Specify input file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;

print fileparse($input)."\n";

my $min_energy = 1000;
my $max_energy = -1000;

my %energies = ();

while (<IN>) {

	my $line = $_;
	
	if ($line =~ /^>.*$species.*/i) { #header
		
		my $seq = <IN>; #sequence
		
		my $str = <IN>; #structure
		chomp $str;
		
		if ($str =~ /(.*)\t(.*)/) {
			my $energy = $2;
			
			$energies{int $energy}++;
			
			if ($energy > $max_energy) {
				$max_energy = $energy;
			}
			if ($energy < $min_energy) {
				$min_energy = $energy;
			}
		} elsif ($str =~ /(.*)\s\((.*)\)/) {
			my $energy = $2;
			
			$energies{int $energy}++;
			
			if ($energy > $max_energy) {
				$max_energy = $energy;
			}
			if ($energy < $min_energy) {
				$min_energy = $energy;
			}
		}
	}
}

close IN;

print "minimum energy\t".$min_energy."\n";
print "maximum energy\t".$max_energy."\n";

foreach my $energy (sort {$a <=> $b} keys %energies) {
	print $energy."\t".$energies{$energy}."\n";
}

print "all\n";
for (my $i = int $min_energy; $i <= int $max_energy; $i++) {
	if (exists $energies{$i}) {
		print $i."\t".$energies{$i}."\n";
	} else {
		print $i."\t0\n";
	}
}