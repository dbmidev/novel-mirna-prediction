#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $output = "";
my $append = 0;
my $category = "";

GetOptions ('input|i=s' => \$input,
			'output|o=s' => \$output,
			'append|a' => \$append,
			'category|c=s' => \$category);

if ($input eq "" || $output eq "" || $category eq "") {
	print "Specify input and/or output file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

#my $triplets = "A..(	A(((	A(.(	A((.	A.(.	A(..	A...	A.((	C..(	C(((	C(.(	C((.	C.(.	C(..	C...	C.((	G..(	G(((	G(.(	G((.	G.(.	G(..	G...	G.((	U..(	U(((	U(.(	U((.	U.(.	U(..	U...	U.((";

open (IN, "<", $input) or die $!;

if ($append) {
	open (OUT, ">>", $output) or die $!;
} else {
	open (OUT, ">", $output) or die $!;
	#print OUT "id\tMFE\tpValue\tstem_pairings\t";#.$triplets."\tcategory\n";
	print OUT "id\tMFE\tstem_pairings\t";#.$triplets."\tcategory\n";
}

my $first = 1;

my $cnt = 1;

while (<IN>) {

	print $cnt."\n";
	$cnt++;

	my $line = $_;
	
	if ($line =~ /^>(.*?)\s/) { #header
		my $header = $1;
		
		my $seq = <IN>; #sequence
		$seq = clean_line($seq); 
		
		my $str = <IN>; #structure
		
		if ($str =~ /(.*)\t(.*)/) {
			my $energy = $2;
			$energy = clean_line($energy);
			$energy = $energy / length($seq);
			
			my $stem_pairings = ($str =~ tr/\(//);
			$stem_pairings = $stem_pairings / length($seq);
			
			my $triplet_header = <IN>;
			
			if ($first) {
				$triplet_header = clean_line($triplet_header);
				print OUT $triplet_header."\tcategory\n";
				$first = 0;
			}
			
			my $triplets = <IN>;
			$triplets = clean_line($triplets);
			
			# open (OUT2, ">", "tmp.txt") or die $!;

			# print OUT2 `perl perl_scripts/generate_random_sequences.pl -i $seq -k 1000`;

			# close OUT2;

			# my $R = 0;

			# system("RNAfold -noPS < tmp.txt > out_tmp.txt");

			# open (IN2, "<", "out_tmp.txt") or die $!;

			# while (<IN2>) {
				# my $line = $_;
				
				# if ($line =~ /^>/) {
					# my $seq = <IN2>;
					# my $str = <IN2>;
					# my $seq_energy = 0.0;
					# if ($str =~ /(.*)\s\((.*)\)/) {
						# $seq_energy = $2;
					# }
					# if ($energy >= $seq_energy) {
						# $R++;
					# }
				# }
			# }
			# close IN2;

			# my $pValue = $R / (1000 + 1);
			
			#print OUT $header."\t".$energy."\t".$pValue."\t".$stem_pairings."\t".$triplets."\t".$category."\n";			
			print OUT $header."\t".$energy."\t".$stem_pairings."\t".$triplets."\t".$category."\n";			
		}	
	}
}

close IN;
close OUT;

sub clean_line {
	my $text = $_[0];
	$text =~ s/[\r\n]//g;
	return $text;
}