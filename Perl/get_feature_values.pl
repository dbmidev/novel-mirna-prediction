#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use Clone qw(clone);

my $input = "";
my $output = "";
my $random = "";
my $iterations = 1000;

GetOptions ('input|i=s' => \$input,
			'output|o=s' => \$output,
			'random|r=s' => \$random,
			'iterations|k=i' => \$iterations);

if ($input eq "" || $output eq "" || $random eq "") {
	print "Specify input and/or output file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

my %mfe_rand = ();

open (RAND, "<", $random) or die $!;

while(<RAND>) {
	my $line = $_;
	
	if ($line =~ /^>(.*?)\s/) { #header
		my $id = $1;
		
		my $seq = <RAND>; #sequence
		$seq = clean_line($seq); 
		
		my $str = <RAND>; #structure
		
		if ($str =~ /(.*)\s[\(](.*)[\)]/) {
			my $energy = $2;
			$energy = clean_line($energy);
			
			push(@{$mfe_rand{$id}}, $energy);
		}	
	}
}

close RAND;

open (IN, "<", $input) or die $!;
open (OUT, ">", $output) or die $!;

my %triplet = ("A" => {"000" => 0, "100" => 0, "010" => 0, "001" => 0, "101" => 0, "011" => 0, "110" => 0, "111" => 0},
			   "C" => {"000" => 0, "100" => 0, "010" => 0, "001" => 0, "101" => 0, "011" => 0, "110" => 0, "111" => 0},
			   "G" => {"000" => 0, "100" => 0, "010" => 0, "001" => 0, "101" => 0, "011" => 0, "110" => 0, "111" => 0},
			   "U" => {"000" => 0, "100" => 0, "010" => 0, "001" => 0, "101" => 0, "011" => 0, "110" => 0, "111" => 0});
			   
my @cat = ("l", "m", "r");

print OUT "id\tseq_len\tMFE\tMFE_pVal\tMFE_norm\tloop_len\tloop_len_norm\tGC_ratio\tGU_wobble\tGU_wobble_norm\tpaired_nt\tunpaired_nt\tunpaired_paired_ratio\tstem3_nt\tstem5_nt\tnt_pairs\tpairs_stem_ratio";

print OUT "\tnum_triplet_l\tnum_triplet_m\tnum_triplet_r";

foreach my $c (@cat) {		
	foreach my $nt (sort keys %triplet) {
		foreach my $t (sort {$a <=> $b} keys %{$triplet{$nt}}) {
			print OUT "\t".$nt.$t."_".$c;
		}
	}
}
print OUT "\n";

while (<IN>) {
	my $line = $_;
	
	if ($line =~ /^>(.*?)\s/) { #header
		my $id = $1;
		
		my $seq = <IN>; #sequence
		$seq = clean_line($seq); 
		
		my $str = <IN>; #structure
		
		if ($str =~ /(.*)\t(.*)/) {
			my $energy = $2;
			$energy = clean_line($energy);
			
			my $R = 0;
			foreach my $mfe (@{$mfe_rand{$id}}) {
				if ($energy >= $mfe) {
					$R++;
				}
			}
		
			my $structure = $1;
			$structure =~ /(\().*\((\.*)\).*(\))/;
			my $stembegin = $-[1];
			my $stemend = $-[3];
			my @stemloop = ($-[2], $+[2]-1);
			my $whole_stem_str = substr($structure, $stembegin, ($stemloop[0] - $stembegin)).substr($structure, $stemloop[1] + 1, ($stemend - $stemloop[1]));
			my $whole_stem_nt = substr($seq, $stembegin, ($stemloop[0] - $stembegin)).substr($seq, $stemloop[1] + 1, ($stemend - $stemloop[1]));
						
			my @a_whole_stem_str = split(//, $whole_stem_str);
			my @a_whole_stem_nt = split(//, $whole_stem_nt);
			
			my $GU_wobble = 0;
			my $i = 0;
			my $j = length($whole_stem_nt) - 1;
			
			while ($i < $j) {
				if (($a_whole_stem_nt[$i] eq "G" && $a_whole_stem_nt[$j] eq "U") ||
				    ($a_whole_stem_nt[$i] eq "U" && $a_whole_stem_nt[$j] eq "G")) {
					$GU_wobble++;
				}
				
				$i++;
				$j--;
				
				while ($a_whole_stem_str[$i] eq ".") {
					$i++;
				}
				while ($a_whole_stem_str[$j] eq ".") {
					$j--;
				}
			}
			
			my %mTriplet = %{clone(\%triplet)};
			my %rTriplet = %{clone(\%triplet)};
			my %lTriplet = %{clone(\%triplet)};
			
			my $num_mtriplets = 0;
			my $num_ltriplets = 0;
			my $num_rtriplets = 0;
	
			for (my $i = 1; $i < length($seq) - 1; $i++) {
				my $structure_triplet = substr($structure, $i - 1, 3);
				$structure_triplet =~ s/\)/\(/g;
				$structure_triplet =~ s/\(/1/g;
				$structure_triplet =~ s/\./0/g;
				
				if (($i >= $stembegin) && ($i < $stemloop[0] || $i > $stemloop[1]) && ($i <= $stemend)) {
					#print substr($seq, $i, 1).$structure_triplet."\n";
					$mTriplet{substr($seq, $i, 1)}{$structure_triplet}++;
					$num_mtriplets++;
				}	

				if (($i > $stembegin) && ($i <= $stemloop[0] || $i > $stemloop[1] + 1) && ($i <= $stemend + 1)) {
					#print substr($seq, $i - 1, 1).$structure_triplet."\n";
					$lTriplet{substr($seq, $i - 1, 1)}{$structure_triplet}++;
					$num_ltriplets++;
				}	

				if (($i >= $stembegin - 1) && ($i < $stemloop[0] - 1 || $i >= $stemloop[1]) && ($i < $stemend)) {
					#print substr($seq, $i + 1, 1).$structure_triplet."\n";
					$rTriplet{substr($seq, $i + 1, 1)}{$structure_triplet}++;
					$num_rtriplets++;
				}					
			}			
			
			my $loop_length = $stemloop[1] - $stemloop[0] + 1;
		
			my $num_C = ($seq =~ tr/C//);
			my $num_G = ($seq =~ tr/G//);
			
			my $paired_nt = ($structure =~ tr/[\(\)]//);
			my $unpaired_nt = ($whole_stem_str =~ tr/\.//);
			my $nt_pairs = ($structure =~ tr/\(//);
			
			my $stem5 = $stemloop[0] - $stembegin;
			my $stem3 = $stemend - $stemloop[1];
			my $stem_max = ($stem5 > $stem3) ? $stem5 : $stem3;
			
			print OUT $id."\t";
			print OUT length($seq)."\t";
			print OUT $energy."\t";
			print OUT ($R/$iterations)."\t";
			print OUT ($energy/length($seq))."\t";
			print OUT $loop_length."\t";
			print OUT ($loop_length/length($seq))."\t";
			print OUT (($num_C + $num_G)/length($seq))."\t";
			print OUT $GU_wobble."\t";
			print OUT ($GU_wobble/$nt_pairs)."\t";
			print OUT $paired_nt."\t";
			print OUT $unpaired_nt."\t";
			print OUT ($unpaired_nt/$paired_nt)."\t";
			print OUT $stem3."\t";
			print OUT $stem5."\t";
			print OUT $nt_pairs."\t";
			print OUT ($nt_pairs/$stem_max)."\t";
			print OUT $num_ltriplets."\t";
			print OUT $num_mtriplets."\t";
			print OUT $num_rtriplets;
			
			foreach my $nt (sort keys %lTriplet) {
				foreach my $triplet (sort {$a <=> $b} keys %{$lTriplet{$nt}}) {
					print OUT "\t".($lTriplet{$nt}{$triplet} / $num_ltriplets);
				}
			}
			foreach my $nt (sort keys %mTriplet) {
				foreach my $triplet (sort {$a <=> $b} keys %{$mTriplet{$nt}}) {
					print OUT "\t".($mTriplet{$nt}{$triplet} / $num_mtriplets);
				}
			}
			foreach my $nt (sort keys %rTriplet) {
				foreach my $triplet (sort {$a <=> $b} keys %{$rTriplet{$nt}}) {
					print OUT "\t".($rTriplet{$nt}{$triplet} / $num_rtriplets);
				}
			}
			print OUT "\n";
		}	
	}
}

close IN;
close OUT;

sub clean_line {
	my $text = $_[0];
	$text =~ s/[\r\n]//g;
	return $text;
}