#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;
use Getopt::Long;

my $usage = << "USAGE";
Description: 
Author: Petra Stepanowsky
Usage: perl reads_with_N_at_posX.pl [options]
Options:
  --help,           --h              help
  --dir,            --d      <dir>   directory containing FASTQ files with extension '.fq' or '.fastq'
  --pos                      <int>   position in read
  --outputdir,      --out    <dir>   output directory
Examples: perl reads_with_N_at_posX.pl --dir samples/ --pos 6
          perl reads_with_N_at_posX.pl --dir samples/ -out samples_N_pos6/
USAGE

my $help =  0;
my $pos = 6;
my $dir = "";
my $output_dir = "./";

# possible command options
GetOptions ('help|h' => $help, 'pos=i' => \$pos, 'dir|d=s' => \$dir, 'outputdir|out=s' => \$output_dir);

if ($help || $dir eq "") {
	print $usage; exit;
}

#my $input_dir = "/storage/kawasakidisease/microrna/fastq_2012/";

my @input_files = @{get_files_from_dir($dir, ".fastq|.fq")};

my $num_files = scalar(@input_files);

for (my  $i = 0; $i < $num_files; $i++) {
	print " ".($i+1)."/".$num_files."\n";
	
	my $basename = fileparse($input_files[$i], (".fastq", ".fq"));
	
	open (IN, "<", $input_files[$i]) or die "Cannot open $input_files[$i]\n";
	open(OUT, ">", $output_dir.$basename."_Npos".$pos.".fastq") or die "Cannot create $output_dir${basename}_Npos${pos}fastq!\n";
	
	while (<IN>) {
		my $header = $_;
		my $seq = <IN>;
		my $header2 = <IN>;
		my $qual = <IN>;
		
		my @chars = split(//, $seq);

		if ($chars[$pos-1] eq "N") {
			print OUT $header;
			print OUT $seq;
			print OUT $header2;
			print OUT $qual;
		}
	}
	
	close OUT;
	close IN;
}




sub get_files_from_dir {
	#open directory handler
	opendir(DIR, $_[0]) or die "Cannot open specified directory $_[0]!\n";
	my @tmp = readdir(DIR);
	@tmp = sort(grep(/$_[1]/, @tmp));
	#close directory handler
	closedir(DIR);
	return add_directory(\@tmp, $_[0]);
}

sub add_directory {
	my @files = @{$_[0]};
	my $dir = $_[1];
	for (my $i = 0; $i < scalar(@files); $i++) {
		$files[$i] = $dir.$files[$i];
	}
	return \@files;
}