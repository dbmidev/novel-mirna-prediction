#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $species = "";
my $output = "";

GetOptions ('input|i=s' => \$input,
			'species|s=s' => \$species,
			'output|o=s' => \$output);

if ($input eq "" || $output eq "") {
	print "Specify input and/or output file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;
open (OUT, ">", $output) or die $!;

print fileparse($input)."\n";

while (<IN>) {

	my $line = $_;
	
	if ($line =~ /^>.*$species.*/i) { #header
		print OUT $line;
		
		my $seq = <IN>; #sequence
		print OUT $seq;
		
		my $str = <IN>; #structure
		print OUT $str;
	}
}

close IN;
close OUT;