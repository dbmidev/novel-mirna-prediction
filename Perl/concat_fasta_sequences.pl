#!/usr/bin/perl
#12/22/2011
#Author: Petra Stepanowsky

use warnings;
use strict;
use Getopt::Long;


my $usage = << "USAGE";
Description: Script to concatenate multiple sequence lines and to convert U's to T's
Author: Petra Stepanowsky
Usage: perl concat_fasta_sequences.pl [options]
Options:
  --help, --h             Help
  --input, --in   <file>  Input file
  --output, --out <file>  Output file
  --u                     convert U's to T's
Examples: perl concat_fasta_sequences.pl --input test.fa --output out.fa
USAGE

my $help   =  0;
my $input  = "";
my $output = "";
my $u      = 0;

GetOptions ('help|h'       => \$help,
			'input|in=s'   => \$input,
			'output|out=s' => \$output,
			'u'            => \$u);
			
###############################
if ($help || $input eq "" || $output eq "") {
	print $usage;
	exit;
}


open (IN, "<", $input) or die "Cannot open input file: $input\n";
open (OUT, ">", $output) or die "Cannot open output file: $output\n";

my $seq = "";

while (<IN>) {
	my $line = $_;
	
	if ($line =~ /^>/) {
		print OUT $seq."\n" if ($seq ne "");
		print OUT $line;
		$seq = "";
	} else {
		chomp $line;
		$line =~ tr/Uu/Tt/ if ($u);
		$seq .= $line;
	}
}
print OUT $seq;

close IN;
close OUT;