#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $species = "";

GetOptions ('input|i=s' => \$input,
			'species|s=s' => \$species);

if ($input eq "") {
	print "Specify input file!\n";
	print "Input file format: \n";
	print "\t>header\n";
	print "\tsequence\n";
	print "\tstructure\tenergy\n";
	exit;
}

open (IN, "<", $input) or die $!;

print fileparse($input)."\n";

my @energyLength = ();

while (<IN>) {

	my $line = $_;
	
	if ($line =~ /^>.*$species.*/i) { #header
		
		my $seq = <IN>; #sequence
		chomp $seq;
		
		my $str = <IN>; #structure
		chomp $str;
		
		if ($str =~ /(.*)\t(.*)/) {
			my $energy = $2;
			
			push(@energyLength, [length($seq), $energy]);
		} elsif ($str =~ /(.*)\s\((.*)\)/) {
			my $energy = $2;
			
			push(@energyLength, [length($seq), $energy]);
		}
	}
}

close IN;

print "length\tenergy\n";

foreach my $pair (@energyLength) {
	print ${$pair}[0]."\t".${$pair}[1]."\n";
}