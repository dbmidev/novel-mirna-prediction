#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use Clone qw(clone);

my $input_file = "";
my $lengths_file = "";
my $output_file = "";

GetOptions ('input|i=s' => \$input_file,
			'lengths|l=s' => \$lengths_file,
			'output|o=s' => \$output_file);

if ($input_file eq "" || $lengths_file eq "" || $output_file eq "") {
	print "Specify input and/or lengths and/or output file!\n";
	print "lengths file format:\n";
	print "\tlength \\t occurence\n";
	print "input file format:\n";
	print "\tone line sequence\n";
	exit;
}

my $sequence = "";
my @lengths = ();

# get sequence
open (IN, "<", $input_file) or die $!;

$sequence = <IN>;

close IN;

my $seq_length = length($sequence);
my $whole_length = 0;

# get length distribution
open(LEN, "<", $lengths_file) or die $!;

while (<LEN>) {
	my $line = $_;
	
	if ($line =~ /^(\d+)\t(\d+)/) {
		for (my $i = 0; $i < $2; $i++) {
			push (@lengths, $1);
		}
		
		$whole_length += $1 * $2;
	}
}

close LEN;

if ($whole_length > $seq_length) {
	print "Can't split the sequence with given length distribution! Sequence is too short!\n";
} else {
	my $cnt = 1;

	open (OUT, ">", $output_file) or die $!;
	for (my $i = 1; $i < ($seq_length / $whole_length); $i++) {
		my @tmp_lengths = @lengths;
		print $i."\n";
		while ($#tmp_lengths >= 0) {
			my $rand = int rand @tmp_lengths;
			my $part_size = splice (@tmp_lengths, $rand, 1); ;
			
			my $part_seq = substr($sequence, 0, $part_size);
			$sequence = substr($sequence, $part_size);
			
			print OUT ">pseudo".$cnt." len=".$part_size."\n";
			print OUT $part_seq."\n";
			
			$cnt++;
		}
	}
	close OUT;
}
print "\n";
print $seq_length."\n";
print $#lengths."\n";
print $whole_length."\n";