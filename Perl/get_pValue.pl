#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use File::Basename;

my $input = "";
my $iterations = 1000;
my $energy = "";

GetOptions ('input|i=s' => \$input,
			'iterations|k=i' => \$iterations,
			'energy|e=f' => \$energy);

if ($input eq "" || $energy eq "") {
	print "Specify input and/or energy value!\n";
	exit;
}

open (OUT, ">", "tmp.txt") or die $!;

print OUT `perl perl_scripts/generate_random_sequences.pl -i $input -k $iterations`;

close OUT;

my $R = 0;

system("RNAfold -noPS < tmp.txt > out_tmp.txt");

open (IN, "<", "out_tmp.txt") or die $!;

while (<IN>) {
	my $line = $_;
	
	if ($line =~ /^>/) {
		my $seq = <IN>;
		my $str = <IN>;
		my $seq_energy = 0.0;
		if ($str =~ /(.*)\s\((.*)\)/) {
			$seq_energy = $2;
		}
		if ($energy >= $seq_energy) {
			$R++;
		}
	}
}
close IN;

my $pValue = $R / ($iterations + 1);
print $pValue;