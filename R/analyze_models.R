library(ROCR)

kfold <- 10

data_tnc.pred <- list()
data_txue.pred <- list()
data_tjiang.pred <- list()
data_tzhao.pred <- list()

data_tnc.perf <- list()
data_txue.perf <- list()
data_tjiang.perf <- list()
data_tzhao.perf <- list()

data_vnc.pred <- list()
data_vxue.pred <- list()
data_vjiang.pred <- list()
data_vzhao.pred <- list()

data_vnc.perf <- list()
data_vxue.perf <- list()
data_vjiang.perf <- list()
data_vzhao.perf <- list()


#######
#analyze logistic regression results
#######

# estimated performance
data_tnc.pred$glm <- prediction(data_tnc.glm$predictions, data_tnc.glm$labels)
data_txue.pred$glm <- prediction(data_txue.glm$predictions, data_txue.glm$labels)
data_tjiang.pred$glm <- prediction(data_tjiang.glm$predictions, data_tjiang.glm$labels)
data_tzhao.pred$glm <- prediction(data_tzhao.glm$predictions, data_tzhao.glm$labels)

data_tnc.perf$glm.auc <- unlist(performance(data_tnc.pred$glm, "auc")@y.values)
data_txue.perf$glm.auc <- unlist(performance(data_txue.pred$glm, "auc")@y.values)
data_tjiang.perf$glm.auc <- unlist(performance(data_tjiang.pred$glm, "auc")@y.values)
data_tzhao.perf$glm.auc <- unlist(performance(data_tzhao.pred$glm, "auc")@y.values)

data_tnc.perf$glm.mauc <- mean(data_tnc.perf$glm.auc)
data_txue.perf$glm.mauc <- mean(data_txue.perf$glm.auc)
data_tjiang.perf$glm.mauc <- mean(data_tjiang.perf$glm.auc)
data_tzhao.perf$glm.mauc <- mean(data_tzhao.perf$glm.auc)

# validation
data_vnc.glm <- list()
data_vxue.glm <- list()
data_vjiang.glm <- list()
data_vzhao.glm <- list()

data_vnc.glm$model <- glm(C ~ ., data=data_tnc, family="binomial")
data_vxue.glm$model <- glm(C ~ ., data=data_txue, family="binomial")
data_vjiang.glm$model <- glm(C ~ ., data=data_tjiang, family="binomial")
data_vzhao.glm$model <- glm(C ~ ., data=data_tzhao, family="binomial")

data_vnc.glm$predictions <- as.vector(predict(data_vnc.glm$model, newdata=data_vnc, type="response"))
data_vxue.glm$predictions <- as.vector(predict(data_vxue.glm$model, newdata=data_vxue, type="response"))
data_vjiang.glm$predictions <- as.vector(predict(data_vjiang.glm$model, newdata=data_vjiang, type="response"))
data_vzhao.glm$predictions <- as.vector(predict(data_vzhao.glm$model, newdata=data_vzhao, type="response"))

data_vnc.glm$labels <- data_vnc[,"C"]
data_vxue.glm$labels <- data_vxue[,"C"]
data_vjiang.glm$labels <- data_vjiang[,"C"]
data_vzhao.glm$labels <- data_vzhao[,"C"]

data_vnc.pred$glm <- prediction(data_vnc.glm$predictions, data_vnc.glm$labels)
data_vxue.pred$glm <- prediction(data_vxue.glm$predictions, data_vxue.glm$labels)
data_vjiang.pred$glm <- prediction(data_vjiang.glm$predictions, data_vjiang.glm$labels)
data_vzhao.pred$glm <- prediction(data_vzhao.glm$predictions, data_vzhao.glm$labels)

data_vnc.perf$glm.auc <- unlist(performance(data_vnc.pred$glm, "auc")@y.values)
data_vxue.perf$glm.auc <- unlist(performance(data_vxue.pred$glm, "auc")@y.values)
data_vjiang.perf$glm.auc <- unlist(performance(data_vjiang.pred$glm, "auc")@y.values)
data_vzhao.perf$glm.auc <- unlist(performance(data_vzhao.pred$glm, "auc")@y.values)


save(data_vnc.glm, file=paste(path, 'RData/data_nc/data_vnc.glm.RData', sep=""))
save(data_vxue.glm, file=paste(path, 'RData/data_xue/data_vxue.glm.RData', sep=""))
save(data_vjiang.glm, file=paste(path, 'RData/data_jiang/data_vjiang.glm.RData', sep=""))
save(data_vzhao.glm, file=paste(path, 'RData/data_zhao/data_vzhao.glm.RData', sep=""))

load(file=paste(path, 'RData/data_nc/data_vnc.glm.RData', sep=""))
load(file=paste(path, 'RData/data_xue/data_vxue.glm.RData', sep=""))
load(file=paste(path, 'RData/data_jiang/data_vjiang.glm.RData', sep=""))
load(file=paste(path, 'RData/data_zhao/data_vzhao.glm.RData', sep=""))


#######
#analyze support vector machine results
#######

# estimated performance

#print_auc <- function(data) {
#    pred <- prediction(data$predictions, data$labels)
#    auc <- unlist(performance(pred, "auc")@y.values)
#    print(auc)
#}

print_auc <- function(data) {
  all_auc <- {}
  for (c in 1:20) {
    if (c < 4 | c > 10 )
    {
      pred <- prediction(data[[c]]$predictions, data[[c]]$labels)
      auc <- unlist(performance(pred, "auc")@y.values)
      print(paste("cost: ", c, sep=" "))
      print(auc)
      all_auc <- rbind(all_auc,auc) 
    }
  }
  return(all_auc)
}

tnc_auc <- print_auc(data_tnc.svm)
txue_auc <- print_auc(data_txue.svm)
tjiang_auc <- print_auc(data_tjiang.svm)
tzhao_auc <- print_auc(data_tzhao.svm)

all_auc_train <- rbind(tnc_auc,txue_auc,tjiang_auc,tzhao_auc)
rows <- c(rep("tnc",13),rep("txue",13),rep("tjiang",13),rep("tzhao",13))
write.csv(all_auc_train, file=paste(path, 'all_auc_train_svm.csv', sep=""), quote=FALSE, row.names = rows)

#load(paste(path, 'RData/data_nc/data_tnc.svm_C10_to_-2.RData', sep=""))
#load(paste(path, 'RData/data_xue/data_txue.svm_C10_to_-1.RData', sep=""))
#load(paste(path, 'RData/data_jiang/data_tjiang.svm_C4.RData', sep=""))
#load(paste(path, 'RData/data_zhao/data_tzhao.svm_C10_to_-1.RData', sep=""))

data_tnc.pred$svm <- prediction(data_tnc.svm[[11]]$predictions, data_tnc.svm[[11]]$labels)
data_txue.pred$svm <- prediction(data_txue.svm[[11]]$predictions, data_txue.svm[[11]]$labels)
data_tjiang.pred$svm <- prediction(data_tjiang.svm[[11]]$predictions, data_tjiang.svm[[11]]$labels)
data_tzhao.pred$svm <- prediction(data_tzhao.svm[[12]]$predictions, data_tzhao.svm[[12]]$labels)

data_tnc.perf$svm.auc <- unlist(performance(data_tnc.pred$svm, "auc")@y.values)
data_txue.perf$svm.auc <- unlist(performance(data_txue.pred$svm, "auc")@y.values)
data_tjiang.perf$svm.auc <- unlist(performance(data_tjiang.pred$svm, "auc")@y.values)
data_tzhao.perf$svm.auc <- unlist(performance(data_tzhao.pred$svm, "auc")@y.values)

data_tnc.perf$svm.mauc <- mean(data_tnc.perf$svm.auc)
data_txue.perf$svm.mauc <- mean(data_txue.perf$svm.auc)
data_tjiang.perf$svm.mauc <- mean(data_tjiang.perf$svm.auc)
data_tzhao.perf$svm.mauc <- mean(data_tzhao.perf$svm.auc)

# validation
data_vnc.svm <- list()
data_vxue.svm <- list()
data_vjiang.svm <- list()
data_vzhao.svm <- list()

data_tnc$C <- as.factor(ifelse(data_tnc$C == 1, "pos", "neg"))
data_txue$C <- as.factor(ifelse(data_txue$C == 1, "pos", "neg"))
data_tjiang$C <- as.factor(ifelse(data_tjiang$C == 1, "pos", "neg"))
data_tzhao$C <- as.factor(ifelse(data_tzhao$C == 1, "pos", "neg"))


data_vnc.svm$model <- svm(C ~ ., data=data_tnc, cost=1, probability = TRUE)
data_vxue.svm$model <- svm(C ~ ., data=data_txue, cost=1, probability = TRUE)
data_vjiang.svm$model <- svm(C ~ ., data=data_tjiang, cost=1, probability = TRUE)
data_vzhao.svm$model <- svm(C ~ ., data=data_tzhao, cost=2, probability = TRUE)

data_vnc.svm$predictions <- attr(predict(data_vnc.svm$model, newdata=data_vnc, probability=TRUE), "probabilities")[,1]
data_vxue.svm$predictions <- attr(predict(data_vxue.svm$model, newdata=data_vxue, probability=TRUE), "probabilities")[,1]
data_vjiang.svm$predictions <- attr(predict(data_vjiang.svm$model, newdata=data_vjiang, probability=TRUE), "probabilities")[,1]
data_vzhao.svm$predictions <- attr(predict(data_vzhao.svm$model, newdata=data_vzhao, probability=TRUE), "probabilities")[,1]

data_vnc.svm$labels <- data_vnc$C
data_vxue.svm$labels <- data_vxue$C
data_vjiang.svm$labels <- data_vjiang$C
data_vzhao.svm$labels <- data_vzhao$C

data_vnc.pred$svm <- prediction(data_vnc.svm$predictions, data_vnc.svm$labels)
data_vxue.pred$svm <- prediction(data_vxue.svm$predictions, data_vxue.svm$labels)
data_vjiang.pred$svm <- prediction(data_vjiang.svm$predictions, data_vjiang.svm$labels)
data_vzhao.pred$svm <- prediction(data_vzhao.svm$predictions, data_vzhao.svm$labels)

data_vnc.perf$svm.auc <- unlist(performance(data_vnc.pred$svm, "auc")@y.values)
data_vxue.perf$svm.auc <- unlist(performance(data_vxue.pred$svm, "auc")@y.values)
data_vjiang.perf$svm.auc <- unlist(performance(data_vjiang.pred$svm, "auc")@y.values)
data_vzhao.perf$svm.auc <- unlist(performance(data_vzhao.pred$svm, "auc")@y.values)

save(data_vnc.svm, file=paste(path, 'RData/data_nc/data_vnc.svm.RData', sep=""))
save(data_vxue.svm, file=paste(path, 'RData/data_xue/data_vxue.svm.RData', sep=""))
save(data_vjiang.svm, file=paste(path, 'RData/data_jiang/data_vjiang.svm.RData', sep=""))
save(data_vzhao.svm, file=paste(path, 'RData/data_zhao/data_vzhao.svm.RData', sep=""))

load(file=paste(path, 'RData/data_nc/data_vnc.svm.RData', sep=""))
load(file=paste(path, 'RData/data_xue/data_vxue.svm.RData', sep=""))
load(file=paste(path, 'RData/data_jiang/data_vjiang.svm.RData', sep=""))
load(file=paste(path, 'RData/data_zhao/data_vzhao.svm.RData', sep=""))

#######
#analyze random forest results
#######

# estimated performance

print_auc <- function(data) {
  all_auc <- {}
  for (m in 1:10) {
    pred <- prediction(data[[m]]$predictions, data[[m]]$labels)
    auc <- unlist(performance(pred, "auc")@y.values)
    print(paste("mtry: ", m, sep=" "))
    print(auc)
    all_auc <- rbind(all_auc,auc)
  }
  return(all_auc)
}

tnc_auc <- print_auc(data_tnc.rf)
txue_auc <- print_auc(data_txue.rf)
tjiang_auc <- print_auc(data_tjiang.rf)
tzhao_auc <- print_auc(data_tzhao.rf)

all_auc_train <- rbind(tnc_auc,txue_auc,tjiang_auc,tzhao_auc)
rows <- c(rep("tnc",10),rep("txue",10),rep("tjiang",10),rep("tzhao",10))
write.csv(all_auc_train, file=paste(path, 'all_auc_train_rf.csv', sep=""), quote=FALSE, row.names = rows)


# validation
data_vnc.rf <- list()
data_vxue.rf <- list()
data_vjiang.rf <- list()
data_vzhao.rf <- list()

data_vnc.rf$model <- randomForest(C ~ ., data=data_tnc, mtry=1, importance=TRUE)
data_vxue.rf$model <- randomForest(C ~ ., data=data_txue, mtry=1, importance=TRUE)
data_vjiang.rf$model <- randomForest(C ~ ., data=data_tjiang, mtry=1, importance=TRUE)
data_vzhao.rf$model <- randomForest(C ~ ., data=data_tzhao, mtry=4, importance=TRUE)

data_vnc.rf$predictions <- predict(data_vnc.rf$model, newdata=data_vnc, type="prob")[,"pos"]
data_vxue.rf$predictions <- predict(data_vxue.rf$model, newdata=data_vxue, type="prob")[,"pos"]
data_vjiang.rf$predictions <- predict(data_vjiang.rf$model, newdata=data_vjiang, type="prob")[,"pos"]
data_vzhao.rf$predictions <- predict(data_vzhao.rf$model, newdata=data_vzhao, type="prob")[,"pos"]

data_vnc.rf$labels <- data_vnc[,"C"]
data_vxue.rf$labels <- data_vxue[,"C"]
data_vjiang.rf$labels <- data_vjiang[,"C"]
data_vzhao.rf$labels <- data_vzhao[,"C"]

data_vnc.pred$rf <- prediction(data_vnc.rf$predictions, data_vnc.rf$labels)
data_vxue.pred$rf <- prediction(data_vxue.rf$predictions, data_vxue.rf$labels)
data_vjiang.pred$rf <- prediction(data_vjiang.rf$predictions, data_vjiang.rf$labels)
data_vzhao.pred$rf <- prediction(data_vzhao.rf$predictions, data_vzhao.rf$labels)

data_vnc.perf$rf.auc <- unlist(performance(data_vnc.pred$rf, "auc")@y.values)
data_vxue.perf$rf.auc <- unlist(performance(data_vxue.pred$rf, "auc")@y.values)
data_vjiang.perf$rf.auc <- unlist(performance(data_vjiang.pred$rf, "auc")@y.values)
data_vzhao.perf$rf.auc <- unlist(performance(data_vzhao.pred$rf, "auc")@y.values)

save(data_vnc.rf, file=paste(path, 'RData/data_nc/data_vnc.rf.RData', sep=""))
save(data_vxue.rf, file=paste(path, 'RData/data_xue/data_vxue.rf.RData', sep=""))
save(data_vjiang.rf, file=paste(path, 'RData/data_jiang/data_vjiang.rf.RData', sep=""))
save(data_vzhao.rf, file=paste(path, 'RData/data_zhao/data_vzhao.rf.RData', sep=""))

load(file=paste(path, 'RData/data_nc/data_vnc.rf.RData', sep=""))
load(file=paste(path, 'RData/data_xue/data_vxue.rf.RData', sep=""))
load(file=paste(path, 'RData/data_jiang/data_vjiang.rf.RData', sep=""))
load(file=paste(path, 'RData/data_zhao/data_vzhao.rf.RData', sep=""))


#######
# compare AUC of models LR, SVM and RF
#######
data_tnc.tt <- list()
data_tnc.pred <- list()
data_tnc.auc <- list()
data_tnc.pred$glm <- prediction(data_tnc.glm$predictions, data_tnc.glm$labels)
data_tnc.auc$glm <- unlist(performance(data_tnc.pred$glm, "auc")@y.values)
data_tnc.pred$svm <- prediction(data_tnc.svm[[20]]$predictions, data_tnc.svm[[20]]$labels)
data_tnc.auc$svm <- unlist(performance(data_tnc.pred$svm, "auc")@y.values)
data_tnc.pred$rf <- prediction(data_tnc.rf[[5]]$predictions, data_tnc.rf[[5]]$labels)
data_tnc.auc$rf <- unlist(performance(data_tnc.pred$rf, "auc")@y.values)

data_tnc.tt$glmsvm <- t.test(data_tnc.auc$glm, data_tnc.auc$svm) 
data_tnc.tt$glmrf <- t.test(data_tnc.auc$glm, data_tnc.auc$rf)
data_tnc.tt$svmrf <- t.test(data_tnc.auc$svm, data_tnc.auc$rf)


data_txue.tt <- list()
data_txue.pred <- list()
data_txue.auc <- list()
data_txue.pred$glm <- prediction(data_txue.glm$predictions, data_txue.glm$labels)
data_txue.auc$glm <- unlist(performance(data_txue.pred$glm, "auc")@y.values)
data_txue.pred$svm <- prediction(data_txue.svm[[14]]$predictions, data_txue.svm[[14]]$labels)
data_txue.auc$svm <- unlist(performance(data_txue.pred$svm, "auc")@y.values)
data_txue.pred$rf <- prediction(data_txue.rf[[6]]$predictions, data_txue.rf[[6]]$labels)
data_txue.auc$rf <- unlist(performance(data_txue.pred$rf, "auc")@y.values)

data_txue.tt$glmsvm <- t.test(data_txue.auc$glm, data_txue.auc$svm) 
data_txue.tt$glmrf <- t.test(data_txue.auc$glm, data_txue.auc$rf)
data_txue.tt$svmrf <- t.test(data_txue.auc$svm, data_txue.auc$rf)


data_tjiang.tt <- list()
data_tjiang.pred <- list()
data_tjiang.auc <- list()
data_tjiang.pred$glm <- prediction(data_tjiang.glm$predictions, data_tjiang.glm$labels)
data_tjiang.auc$glm <- unlist(performance(data_tjiang.pred$glm, "auc")@y.values)
data_tjiang.pred$svm <- prediction(data_tjiang.svm[[1]]$predictions, data_tjiang.svm[[1]]$labels)
data_tjiang.auc$svm <- unlist(performance(data_tjiang.pred$svm, "auc")@y.values)
data_tjiang.pred$rf <- prediction(data_tjiang.rf[[10]]$predictions, data_tjiang.rf[[10]]$labels)
data_tjiang.auc$rf <- unlist(performance(data_tjiang.pred$rf, "auc")@y.values)

data_tjiang.tt$glmsvm <- t.test(data_tjiang.auc$glm, data_tjiang.auc$svm) 
data_tjiang.tt$glmrf <- t.test(data_tjiang.auc$glm, data_tjiang.auc$rf)
data_tjiang.tt$svmrf <- t.test(data_tjiang.auc$svm, data_tjiang.auc$rf)


data_tzhao.tt <- list()
data_tzhao.pred <- list()
data_tzhao.auc <- list()
data_tzhao.pred$glm <- prediction(data_tzhao.glm$predictions, data_tzhao.glm$labels)
data_tzhao.auc$glm <- unlist(performance(data_tzhao.pred$glm, "auc")@y.values)
data_tzhao.pred$svm <- prediction(data_tzhao.svm[[17]]$predictions, data_tzhao.svm[[17]]$labels)
data_tzhao.auc$svm <- unlist(performance(data_tzhao.pred$svm, "auc")@y.values)
data_tzhao.pred$rf <- prediction(data_tzhao.rf[[10]]$predictions, data_tzhao.rf[[10]]$labels)
data_tzhao.auc$rf <- unlist(performance(data_tzhao.pred$rf, "auc")@y.values)

data_tzhao.tt$glmsvm <- t.test(data_tzhao.auc$glm, data_tzhao.auc$svm) 
data_tzhao.tt$glmrf <- t.test(data_tzhao.auc$glm, data_tzhao.auc$rf)
data_tzhao.tt$svmrf <- t.test(data_tzhao.auc$svm, data_tzhao.auc$rf)

#######
# save results
#######
save(data_tnc.tt, file=paste(path, 'RData/data_nc/data_tnc.tt.RData', sep=""))
save(data_txue.tt, file=paste(path, 'RData/data_xue/data_txue.tt.RData', sep=""))
save(data_tjiang.tt, file=paste(path, 'RData/data_jiang/data_tjiang.tt.RData', sep=""))
save(data_tzhao.tt, file=paste(path, 'RData/data_zhao/data_tzhao.tt.RData', sep=""))

save(data_tnc.pred, file=paste(path, 'RData/data_nc/data_tnc.pred.RData', sep=""))
save(data_txue.pred, file=paste(path, 'RData/data_xue/data_txue.pred.RData', sep=""))
save(data_tjiang.pred, file=paste(path, 'RData/data_jiang/data_tjiang.pred.RData', sep=""))
save(data_tzhao.pred, file=paste(path, 'RData/data_zhao/data_tzhao.pred.RData', sep=""))

save(data_tnc.auc, file=paste(path, 'RData/data_nc/data_tnc.auc.RData', sep=""))
save(data_txue.auc, file=paste(path, 'RData/data_xue/data_txue.auc.RData', sep=""))
save(data_tjiang.auc, file=paste(path, 'RData/data_jiang/data_tjiang.auc.RData', sep=""))
save(data_tzhao.auc, file=paste(path, 'RData/data_zhao/data_tzhao.auc.RData', sep=""))

#######
# load results
#######
load(paste(path, 'RData/data_nc/data_tnc.tt.RData', sep=""))
load(paste(path, 'RData/data_xue/data_txue.tt.RData', sep=""))
load(paste(path, 'RData/data_jiang/data_tjiang.tt.RData', sep=""))
load(paste(path, 'RData/data_zhao/data_tzhao.tt.RData', sep=""))

load(paste(path, 'RData/data_nc/data_tnc.pred.RData', sep=""))
load(paste(path, 'RData/data_xue/data_txue.pred.RData', sep=""))
load(paste(path, 'RData/data_jiang/data_tjiang.pred.RData', sep=""))
load(paste(path, 'RData/data_zhao/data_tzhao.pred.RData', sep=""))

load(paste(path, 'RData/data_nc/data_tnc.auc.RData', sep=""))
load(paste(path, 'RData/data_xue/data_txue.auc.RData', sep=""))
load(paste(path, 'RData/data_jiang/data_tjiang.auc.RData', sep=""))
load(paste(path, 'RData/data_zhao/data_tzhao.auc.RData', sep=""))


#######
# draw boxplot
#######
data.glm <- cbind(data_tnc.auc$glm, data_txue.auc$glm, data_tjiang.auc$glm, data_tzhao.auc$glm)
colnames(data.glm) <- c("my", "Xue", "Jiang", "Zhao")
data.svm <- cbind(data_tnc.auc$svm , data_txue.auc$svm , data_tjiang.auc$svm , data_tzhao.auc$svm)
colnames(data.svm) <- c("my", "Xue", "Jiang", "Zhao")
data.rf <- cbind(data_tnc.auc$rf, data_txue.auc$rf, data_tjiang.auc$rf, data_tzhao.auc$rf)
colnames(data.rf) <- c("my", "Xue", "Jiang", "Zhao")

png(file=paste(path, "boxplot_glm.png", sep=""), bg="white")
boxplot(data.glm, ylim=c(0.9, 1), main="logistic regression", ylab="AUC", xlab="classifier models")
dev.off()
png(file=paste(path, "boxplot_svm.png", sep=""), bg="white")
boxplot(data.svm, ylim=c(0.85, 1), main="support vector machine", ylab="AUC", xlab="classifier models")
dev.off()
png(file=paste(path, "boxplot_rf.png", sep=""), bg="white")
boxplot(data.rf, ylim=c(0.9, 1), main="random forest", ylab="AUC", xlab="classifier models")
dev.off()


#######
# most important features
#######

top <- 30

# glm
nc.glm.pval <- summary(data_vnc.glm$model)$coeff[-1,][,4]
nc.glm.pval <- c(nc.glm.pval[1:28], 1, nc.glm.pval[29:30], 1)
nc.glm.pval <- nc.glm.pval[-c(30,31)]
names(nc.glm.pval)[29:30] <- c("stem5_nt","nt_pairs")
xue.glm.pval <- summary(data_vxue.glm$model)$coeff[-1,][,4]
xue.glm.pval <- c(xue.glm.pval[1:28], 1, xue.glm.pval[29:30], 1)
jiang.glm.pval <- summary(data_vjiang.glm$model)$coeff[-1,][,4]
jiang.glm.pval <- c(jiang.glm.pval[1:30], 1, jiang.glm.pval[31:32], 1)
zhao.glm.pval <- summary(data_vzhao.glm$model)$coeff[-1,][,4]
zhao.glm.pval <- c(zhao.glm.pval, 1)


# svm
nc.svm.coef <- abs(t(data_vnc.svm$model$coefs) %*% data_vnc.svm$model$SV)
xue.svm.coef <- abs(t(data_vxue.svm$model$coefs) %*% data_vxue.svm$model$SV)
jiang.svm.coef <- abs(t(data_vjiang.svm$model$coefs) %*% data_vjiang.svm$model$SV)
zhao.svm.coef <- abs(t(data_vzhao.svm$model$coefs) %*% data_vzhao.svm$model$SV)


# rf
nc.rf.coef <- importance(data_vnc.rf$model)[,3]
xue.rf.coef <- importance(data_vxue.rf$model)[,3]
jiang.rf.coef <- importance(data_vjiang.rf$model)[,3]
zhao.rf.coef <- importance(data_vzhao.rf$model)[,3]



varImpPlot(data_vnc.rf$model)
rf.coef <- importance(data_vnc.rf$model)
rf.ord <- order(rf.coef, decreasing=TRUE)
as.data.frame(rf.coef[rf.ord[1:top]])


varImpPlot(data_vjiang.rf$model)

#######
# t-test within model
#######

# glm
t.nc_xue <- t.test(data_tnc.auc$glm, data_txue.auc$glm, paired=T)
t.nc_jiang <- t.test(data_tnc.auc$glm, data_tjiang.auc$glm, paired=T)
t.nc_zhao <- t.test(data_tnc.auc$glm, data_tzhao.auc$glm, paired=T)

pdf(file=paste(path, "boxplot_glm.pdf", sep=""), width=20, height=10)
par(mfrow=c(1,2))
boxplot(data_txue.perf$glm.auc, data_tjiang.perf$glm.auc, data_tzhao.perf$glm.auc, data_tnc.perf$glm.auc, 
        names=c("Xue", "Jiang", "Zhao", "Proposed method"), main="Logistic regression",
        ylab="AUC", boxwex = 0.5, ylim=c(0.9, 1))
boxplot(data_txue.auc$rf, data_tjiang.auc$rf, data_tzhao.auc$rf, data_tnc.auc$rf, 
        names=c("Xue", "Jiang", "Zhao", "Proposed method"), main="Random forest", boxwex=0.5,
        ylab="AUC", ylim=c(0.9, 1))
dev.off()


# rf

#data_tnc.auc <- c(0.9827426, 0.9685704, 0.976726, 0.9865748, 0.9734242, 0.971069, 0.9761455, 0.9720999, 0.972226, 0.9745884)
#data_txue.auc <- c(0.9343123, 0.9361595, 0.9297042, 0.9284704, 0.9403311, 0.9215362, 0.9059671, 0.9066196, 0.9471709, 0.9312211)
#data_tjiang.auc <- c(0.9703026, 0.9777049, 0.9779141, 0.9624687, 0.9708119, 0.9675206, 0.9610009, 0.9838173, 0.9727596, 0.9729512)
#data_tzhao.auc <- c(0.9812501, 0.9762931, 0.9809806, 0.9725243, 0.9482939, 0.9712728, 0.973788, 0.9720562, 0.9650806, 0.9714547)


t.nc_xue <- t.test(data_tnc.auc$rf, data_txue.auc$rf, paired=T)
t.nc_jiang <- t.test(data_tnc.auc$rf, data_tjiang.auc$rf, paired=T)
t.nc_zhao <- t.test(data_tnc.auc$rf, data_tzhao.auc$rf, paired=T)

png(file=paste(path, "boxplot_rf.png", sep=""), bg="white")
boxplot(data_txue.auc$rf, data_tjiang.auc$rf, data_tzhao.auc$rf, data_tnc.auc$rf, 
        names=c("Xue et al.", "Jiang et al.", "Zhao et al.", "our method"), main="random forest",
        ylab="AUC", ylim=c(0.9, 1))
dev.off()



# svm
t.nc_xue <- t.test(data_tnc.auc$svm, data_txue.auc$svm, paired=T)
t.nc_jiang <- t.test(data_tnc.auc$svm, data_tjiang.auc$svm, paired=T)
t.nc_zhao <- t.test(data_tnc.auc$svm, data_tzhao.auc$svm, paired=T)

png(file=paste(path, "boxplot_svm.png", sep=""), bg="white")
boxplot(data_txue.perf$svm.auc, data_tjiang.perf$svm.auc, data_tzhao.perf$svm.auc, data_tnc.perf$svm.auc,
        names=c("Xue et al.", "Jiang et al.", "Zhao et al.", "our method"), main="support vector machine",
        ylab="AUC", ylim=c(0.9, 1))
dev.off()



#######
# draw CAT plot
#######

comp2 <- list()
comp2[[1]] <- cbind(nc.glm.pval, nc.svm.coef[1,], nc.rf.coef)
comp2[[2]] <- cbind(xue.glm.pval, xue.svm.coef[1,], xue.rf.coef)
comp2[[3]] <- cbind(jiang.glm.pval, jiang.svm.coef[1,], jiang.rf.coef)
comp2[[4]] <- cbind(zhao.glm.pval, zhao.svm.coef[1,], zhao.rf.coef)
colnames(comp2[[1]]) <- c("LR", "SVM", "RF")
colnames(comp2[[2]]) <- c("LR", "SVM", "RF")
colnames(comp2[[3]]) <- c("LR", "SVM", "RF")
colnames(comp2[[4]]) <- c("LR", "SVM", "RF")



### Correspondence At the Top (CAT)
get.cat <- function(x, sz, tieHandle="max")
  {
    sel.numer <- rank(x[,1], ties.method=tieHandle) <= sz
    topgenes.numer <- rownames(x)[sel.numer]

    sel.denom <- rank(x[,2] * -1, ties.method=tieHandle) <= sz
    topgenes.denom <- rownames(x)[sel.denom]

    sel.denom2 <- rank(x[,3] * -1, ties.method=tieHandle) <= sz
    topgenes.denom2 <- rownames(x)[sel.denom2]    

    cat.ratio <-  length(intersect(intersect(topgenes.denom, topgenes.numer), topgenes.denom2)) / sz
    return(cat.ratio)
  }



### GENERATE THE CORRESPONDENCE AT THE TOP (CAT) SET
listSize <- seq(5, 36, 1)
len <- length(listSize)
CATcorrespondence <- list()
for(i in 1:4) {
  CATcorrespondence[[i]] <- list()
  for(j in 1:len) {
    if (i == 1 && listSize[j] <= 30) {
      CATcorrespondence[[i]][j] <- get.cat(comp2[[i]], listSize[j], "min")
    }
    if (i == 2 && listSize[j] <= 32) {
      CATcorrespondence[[i]][j] <- get.cat(comp2[[i]], listSize[j], "min")
    }
    if (i == 3 && listSize[j] <= 34) {
      CATcorrespondence[[i]][j] <- get.cat(comp2[[i]], listSize[j], "min")
    }
    if (i == 4 && listSize[j] <= 36) {
      CATcorrespondence[[i]][j] <- get.cat(comp2[[i]], listSize[j], "min")
    }
  }
}


### DRAW A CAT PLOT
png(file=paste(path, "CAT_plot.png", sep=""), bg="white")
plot(5:36, CATcorrespondence[[4]], type="b",  lwd=2, col="tomato", ylim=c(0, 1),
      ylab="Proportion in common", xlab="Size of list")
points(5:32, CATcorrespondence[[2]], type="b", lwd=2, col="orange2")
points(5:34, CATcorrespondence[[3]], type="b", lwd=2, col="blue")
points(5:30, CATcorrespondence[[1]], type="b", lwd=2, col="black")


legend(24, 0.2, legend=c("our method", "Xue et al.",   "Jiang et al.", "Zhao et al."),
        lty=c(1, 1, 1, 1),       
        col=c(  "black", "orange2",   "blue",  "tomato") )
dev.off()








































perf <- list()
perf <- calc_perf_values(data_vzhao.glm$model, data_vzhao.glm$predictions, data_vzhao.glm$labels, perf, 10)
perf

#######
# calculate sensitivity, specificity, accuracy, error-rate
#######
calc_perf_values <- function(data.model, data.pred, data.labels, data.perf.m, kfold=10) {
  #for (i in 1:kfold) {
    preds <- data.pred#[[i]]
    labs <- data.labels#[[i]]
    tab <- table(preds>0.5, labs)
    print(tab)
    a <- tab[2,2]
    b <- tab[2,1]
    c <- tab[1,2]
    d <- tab[1,1]
  
    #data.perf.m$sens[[i]] <- a / (a + c)
   # data.perf.m$spec[[i]] <- d / (b + d)
   # data.perf.m$acc[[i]] <- (a + d) / (a + b + c + d)
    #data.perf.m$err[[i]] <- (c + b) / (a + b + c + d)

    data.perf.m$sens <- a / (a + c)
    data.perf.m$spec <- d / (b + d)
    data.perf.m$acc <- (a + d) / (a + b + c + d)
    data.perf.m$err <- (c + b) / (a + b + c + d)

 # }

  return(data.perf.m)
}

#######
# draw boxplot
#######
draw_bp_pval <- function(filename, title, glm.auc, svm.auc, rf.auc, svmglm.p, rfglm.p, rfsvm.p) {
  png(file=filename, bg="white")
  data.bp <- boxplot(glm.auc, svm.auc, rf.auc, ylim=c(0.9, 1), names=c("LR", "SVM", "RF"), main=title, ylab="AUC", xlab="classifier models")
  segments(1, data.bp$stats[3,1], 2, data.bp$stats[3,2], col="blue")
  segments(1, data.bp$stats[3,1], 3, data.bp$stats[3,3], col="blue")
  segments(2, data.bp$stats[3,2], 3, data.bp$stats[3,3], col="blue")
  text(1.4, data.bp$stats[3,1]-0.015, labels=round(svmglm.p, 4), col="blue")
  text(2.6, data.bp$stats[3,1]-0.02, labels=round(rfsvm.p, 4), col="blue")
  text(2, data.bp$stats[3,1]+0.005, labels=round(rfglm.p, 4), col="blue")
  dev.off()

  return(data.bp)
}
#######
# draw ROC
#######
draw_roc <- function(filename, title, pred.glm, pred.svm, pred.rf) {
  perf.glm <- performance(pred.glm, "tpr", "fpr")
  perf.svm <- performance(pred.svm, "tpr", "fpr")
  perf.rf <- performance(pred.rf, "tpr", "fpr")
  png(file=filename, bg="white")
  plot(perf.glm, avg="vertical", col=2, main=title)
  plot(perf.svm, avg="vertical", col=3, add=T)
  plot(perf.rf, avg="vertical", col=4, add=T)
  legend(0.6, 0.6, c("LR", "SVM", "RF"), c(2:4))
  dev.off()
}
